const {ccclass, property} = cc._decorator;

@ccclass
export default class MenuSceneController extends cc.Component {
    
    onLoad() {
 
    }

    //set button events
    onPlayButton(){
        cc.director.loadScene("GameScene");
    }

    onQuitButton(){
        cc.game.end();
    }

    toHighScoreScene(){
        cc.director.loadScene("HighScore");
    }

}
