import {Balloon} from "./Balloon"; 

const {ccclass, property} = cc._decorator;

@ccclass
export class GameSceneController extends cc.Component {
    //set properties
    @property(cc.Prefab)
    balloonePrefab : cc.Prefab = null;

    @property(cc.Label)
    scorealbel : cc.Label = null;

    @property(cc.Node)
    menuButton :cc.Node = null;

    @property(cc.ProgressBar)
    lifeBar : cc.ProgressBar = null;

    @property
    balloonInterval : number = 0.5;
    
    //pause menu node
    private pauseMenu : cc.Node = null;
    //instanite baloon
    private ball : cc.Node = null;
    private timer : number = 5;

    private visibleSize : cc.Vec2;

    public static score : number = 0;
    public static life : number = 1;
    public static pause : boolean = false;
    
    start () {
        this.visibleSize = new cc.Vec2(this.node.width, this.node.height);
        
        //set  static property
        GameSceneController.pause = false;
        GameSceneController.score = 0;
        GameSceneController.life = 1;
        
        //set start position GUI elements
        this.pauseMenu = this.node.getChildByName("PauseMenu");
        this.pauseMenu.setLocalZOrder(99);
        this.scorealbel.node.setPosition(new cc.Vec2(this.visibleSize.x - 130, this.visibleSize.y -40));
        this.menuButton.setPosition(new cc.Vec2(this.visibleSize.x - 80, 80));
        this.lifeBar.node.setPosition(new cc.Vec2(70, this.visibleSize.y -40))
    }


     update (dt) {
         //if life is over go to the gameover scene
        if(GameSceneController.life < 0.1){
            this.toGameOverScene();
        }
        //if game not pause run baloon
        if(!GameSceneController.pause){
            this.balloonRunUp(dt);
        }
        //init lifrbar and score gui;
        this.lifeBar.progress = GameSceneController.life;
        this.scorealbel.string = "score: " + GameSceneController.score;
     }
     //random color animation
    private randomColor(animation : cc.Animation) : void{
        let rand = Math.floor(Math.random() * 10);
        switch(rand){
            case 0:
                animation.play("red");
                break;
            case 1:
                animation.play("orange");
                break;
            case 2:
                animation.play("yellow");
                break;
            case 3:
                animation.play("green");
                break;
            case 4:
                animation.play("blue");
                break;
            case 5:
                animation.play("indigo");
                break;
            case 6:
                animation.play("violet");
                break;
            default:
                animation.play("wrong");
                break;
        }
    }

    //set start x ballon position
    private randomPosition() : number{
        let rand = Math.random() * ((this.node.width /2 -this.ball.width /2) - (-this.node.width /2 + this.ball.width / 2)) + (-this.node.width /2 + this.ball.width /2);

        return rand; 
    }
    //run balloon 
    private balloonRunUp(dt) : void{
        this.timer += dt;
        //run balls at interval
        if (this.timer > this.balloonInterval) {
            this.ball = cc.instantiate(this.balloonePrefab);
            let anim = this.ball.getComponent(cc.Animation);
            this.randomColor(anim);
            this.node.addChild(this.ball);
            this.ball.setPosition(new cc.Vec2(this.randomPosition(), -this.node.height /2 -this.ball.height /2 ) );
            this.timer = 0;
        }
    }

    //set buttons events
    onPauseButton() : void{

        GameSceneController.pause ? GameSceneController.pause = false : GameSceneController.pause = true;
        this.pauseMenu.active ? this.pauseMenu.active = false : this.pauseMenu.active = true; 
    }

    toMainMenu() : void{

        cc.director.loadScene("MainMenu");

    }

    toGameOverScene() : void{
        cc.director.loadScene("GameOver");
    }

    restart() : void{
        cc.director.loadScene("GameScene");
    }
}
