import {HighScoreController} from "./HighScoreController"
import { GameSceneController } from "./GameSceneController";
const {ccclass, property} = cc._decorator;

@ccclass
export default class GameOverController extends cc.Component {
    
    @property(cc.Node)
    setScoreLayout : cc.Node = null;

    @property(cc.Label)
    scoreLabel : cc.Label = null;

    @property(cc.EditBox)
    nameEditBox : cc.EditBox = null;

    highScore: HighScoreController = null;

    name : string = null;
    // onLoad () {}

    start () {
        this.highScore = new HighScoreController();
        this.setScoreLayout.active = false;

        if( !JSON.parse(cc.sys.localStorage.getItem('highScoreNames')) ){
            this.highScore.startSetStorage();
        }
        
        if (GameSceneController.score > this.getMinScore()){

            this.scoreLabel.string = GameSceneController.score + "";
            this.setScoreLayout.active = true;
        }
    }

    public toMainMenu() : void{
        cc.director.loadScene("MainMenu");
    }

    public retry() : void {
        cc.director.loadScene("GameScene");
    }

    public toHighScore() : void{
        cc.director.loadScene("HighScore");
    }

    private OnSendButton() : void {
        if (this.nameEditBox.string == "")
            this.name = "<unnamed>";
        else
        this.name = this.nameEditBox.string;
        this.setScoreToHighScore();
        //this.setScoreLayout.active = false;
        cc.director.loadScene("HighScore");
    }

    
    private setScoreToHighScore() : void {
        let highScoreArr = JSON.parse(cc.sys.localStorage.getItem('highScoreValue'));
        let highScoreNamesArr = JSON.parse(cc.sys.localStorage.getItem('highScoreNames'));


        for(let i = 0; i< highScoreArr.length; ++i){
            if(GameSceneController.score > highScoreArr[i]){
                highScoreArr.splice(i, 0, GameSceneController.score);
                highScoreNamesArr.splice(i, 0, this.name);
                highScoreArr.splice(-1, 1);
                highScoreNamesArr.splice(-1,1);
                break;
            }
        }
        this.setStorage(highScoreNamesArr, highScoreArr);
    }
    
    public setStorage(names : string[], score: number[] ) : void {
        cc.sys.localStorage.setItem('highScoreNames', JSON.stringify(names));
        cc.sys.localStorage.setItem('highScoreValue', JSON.stringify(score));


    }

    public getMinScore() : number{
        var res = JSON.parse(cc.sys.localStorage.getItem('highScoreValue'));
        return res[9];
    }


}
