const {ccclass, property} = cc._decorator;

@ccclass
export class HighScoreController extends cc.Component {

    @property(cc.Button)
    backButton : cc.Button = null;
    
    @property([cc.Label])
    nameLabels : cc.Label[] = [];

    @property([cc.Label])
    scoreLabels : cc.Label[] = [];

    private names : string[] = [];
    private score : number[] = [];

    start () {

        this.backButton.node.setPosition(this.node.width - 50, 50);

        if( !cc.sys.localStorage.getItem('highScoreNames') ){
            this.startSetStorage();
            this.getStorage();
        } else {
            this.getStorage();
        }
    }

    // update (dt) {}

    public toMainMenu() : void {
        cc.director.loadScene("MainMenu");
    }

    public startSetStorage() : void{
        this.labelToValue();
        console.log(this.names);
        console.log(this.score);
        cc.sys.localStorage.setItem('highScoreNames', JSON.stringify(this.names));
        cc.sys.localStorage.setItem('highScoreValue', JSON.stringify(this.score));
    }

    public getStorage() : void {
        
        this.names = JSON.parse(cc.sys.localStorage.getItem('highScoreNames'));
        this.score = JSON.parse(cc.sys.localStorage.getItem('highScoreValue'));
        console.log(this.names);
        console.log(this.score);

        this.valueToLabel();
    }
    //init value array from label at scene
    private labelToValue() :void {
        for(let i = 0; i < this.nameLabels.length; ++i){
            this.names[i] = this.nameLabels[i].string;
            this.score[i] = +this.scoreLabels[i].string;
        }
    }
    //init label from value array
    private valueToLabel() : void{
        for(let i = 0; i < this.names.length; ++i){
            this.nameLabels[i].string = this.names[i];
            this.scoreLabels[i].string = this.score[i] + "";
        }
    }
}
