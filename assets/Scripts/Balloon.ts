import {GameSceneController} from "./GameSceneController";
const {ccclass, property} = cc._decorator;

@ccclass
export class Balloon extends cc.Component {

    @property
    moveSpeed : number = 50;

    wrongSound : string = null;
    
    private notDestroy : boolean = true;

    start () {
        this.moveSpeed = Math.random() * (250 - 100) + 100;
        let scale = Math.random() * 3 + 1;
        this.node.setScale(scale);
        this.node.setLocalZOrder(scale);
        
        //this.wrongSound = "res/raw-assets/Audio/beep.mp3";
        this.wrongSound = cc.url.raw("Audio/beep.mp3");
    }

    update(dt){
        //if bolloon not destroy, go up
        if(this.notDestroy && !GameSceneController.pause){
            this.node.setPositionY(this.node.position.y += this.moveSpeed * dt);
        }

        if( this.node.position.y > this.node.parent.height / 2 + this.node.height /2 ) {
            if( this.getComponent(cc.Animation).currentClip.name != "wrong"){
                GameSceneController.life -= 0.1;
            }
            this.node.destroy();
        }
    }


     onLoad () {
        this.node.on(cc.Node.EventType.TOUCH_START, (event: cc.Touch) => {
            if(!GameSceneController.pause){
                this.notDestroy = false;
                this.node.getComponentInChildren(cc.ParticleSystem).resetSystem();
                
            
                if( this.getComponent(cc.Animation).currentClip.name == "wrong"){
                    GameSceneController.life -= 0.1;
                    cc.audioEngine.play(this.wrongSound,false,0.8);
                } else{
                    this.getComponent(cc.AudioSource).play();
                    GameSceneController.score++;
                }
                this.scheduleOnce(()=>{this.node.destroy()}, 0.2);
            }
        }, this);
     }
}
